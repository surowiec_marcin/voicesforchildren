package vege.voicesforchildren;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.widget.Toast;

/**
 * Created by Vege on 03.08.2017.
 */

public class MyReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

    }

    public class MojKursowyReceiver extends BroadcastReceiver {

        public static final String MY_PREFS = "MOJ_KURSIK";
        public static final String MUZYKA = "kursMusicKey";

        @Override
        public void onReceive(Context context, Intent intent) {

            SharedPreferences sharedPreferences = context.getSharedPreferences(MY_PREFS, Context.MODE_PRIVATE);
            boolean graj = sharedPreferences.getBoolean(MUZYKA, false);


            if(intent.getAction().equals("android.intent.action.ACTION_POWER_CONNECTED") && graj == true){

                Toast.makeText(context, "AKCJA 1 Z LADOWANIEM", Toast.LENGTH_SHORT).show();
                Intent i = new Intent(context, MusicService.class);
                context.startService(i);
            }

            if(intent.getAction().equals("android.intent.action.ACTION_POWER_DISCONNECTED")){
                Toast.makeText(context, "Telefon został odłączony od ładowarki", Toast.LENGTH_SHORT).show();

                Intent i = new Intent(context, MusicService.class);
                context.stopService(i);
            }


//        if(intent.getAction().equals("android.intent.action.ACTION_POWER_CONNECTED")){
//
//                Toast.makeText(context, "Telefon został podłączony do ładowarki", Toast.LENGTH_LONG).show();
//                Intent i = new Intent(context, MusicService.class);
//                context.startService(i);
//
//
//        }

//        if(intent.getAction().equals("android.intent.action.ACTION_POWER_DISCONNECTED")){
//            Toast.makeText(context, "Telefon został odłączony od ładowarki", Toast.LENGTH_LONG).show();
//            Intent i = new Intent(context, MusicService.class);
//            context.stopService(i);
//        }

        }
    }}
