package vege.voicesforchildren;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class Info extends AppCompatActivity {

    @BindView(R.id.infoLy)LinearLayout infoLL;

    String[] linki;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
        ButterKnife.bind(this);

        //String[] linkiNazw=new String[50];
        String[] linki={
                "http://icons8.com/tag/media-controls/",
                        "https://icons8.com/icon/2770/vacuum-cleaner" ,
                        "https://icons8.com/icon/24750/blender" ,
                        "https://icons8.com/icon/13885/dishwasher" ,
                        "https://icons8.com/icon/49380/washing-machine" ,
                        "https://icons8.com/icon/549/washing-machine" ,
                        "https://icons8.com/icon/71/hair-dryer" ,
                        "https://icons8.com/icon/71/hair-dryer#filled" ,
                        "https://icons8.com/icon/54374/hair-dryer" ,
                        "https://icons8.com/icon/4455/bee" ,
                        "https://icons8.com/icon/5064/falcon" ,
                        "https://icons8.com/icon/44131/parrot" ,
                        "https://icons8.com/icon/36450/owl" ,
                        "https://icons8.com/icon/12695/bird#filled" ,
                        "https://icons8.com/icon/12695/bird" ,
                        "https://icons8.com/icon/16481/stork" ,
                        "https://icons8.com/icon/3104/fire-element" ,
                        "https://icons8.com/icon/2512/campfire" ,
                        "https://icons8.com/icon/2512/campfire#filled" ,
                        "https://icons8.com/icon/794/leaf" ,
                        "https://icons8.com/icon/794/leaf#filled" ,
                        "https://icons8.com/icon/2427/waterfall" ,
                        "https://icons8.com/icon/2427/waterfall#filled" ,
                        "https://icons8.com/icon/6600/grasshopper" ,
                        "https://icons8.com/icon/18582/knitted-frog" ,
                        "https://icons8.com/icon/48737/leaf" ,
                        "https://icons8.com/icon/2497/forest" ,
                        "https://icons8.com/icon/2497/forest#filled" ,
                        "https://icons8.com/icon/3023/sea-waves" ,
                        "https://icons8.com/icon/3023/sea-waves#filled" ,
                        "https://icons8.com/icon/23756/sea-waves" ,
                        "https://icons8.com/icon/3611/rainy-weather" ,
                        "https://icons8.com/icon/3611/rainy-weather#filled" ,
                        "https://icons8.com/icon/4819/plumbing" ,
                        "https://icons8.com/icon/9141/shower#filled" ,
                        "https://icons8.com/icon/9141/shower" ,
                        "https://icons8.com/icon/3432/wet" ,
                        "https://icons8.com/icon/4886/piping" ,
                        "https://icons8.com/icon/5289/field" ,
                        "https://icons8.com/icon/5289/field#filled" ,
                        "https://icons8.com/icon/2620/creek" ,
                        "https://icons8.com/icon/2620/creek#filled" ,
                        "https://icons8.com/icon/2638/keep-dry#filled" ,
                        "https://icons8.com/icon/2638/keep-dry" ,
                        "https://icons8.com/icon/11489/jacuzzi" ,
                        "https://icons8.com/icon/11485/shower-and-tub" ,
                        "https://icons8.com/icon/53931/shower-and-tub" ,
                        "https://icons8.com/icon/5008/wharf" ,
                        "https://icons8.com/icon/18564/intense-rain" ,
                        "https://icons8.com/icon/18565/moderate-rain" ,
                        "https://icons8.com/icon/51911/steam-engine" ,
                        "https://icons8.com/icon/7358/light-rain" ,
                        "https://icons8.com/icon/18563/heavy-rain" ,
                        "https://icons8.com/icon/49635/rain" ,
                        "https://icons8.com/icon/836/chance-of-storm" ,
                        "https://icons8.com/icon/1770/hydroelectric" ,
                        "https://icons8.com/icon/4461/dragonfly" ,
                        "https://icons8.com/icon/5291/hay",
                "https://icons8.com/icon/398/play",
                        "https://icons8.com/icon/11504/forward-arrow",
                        "https://icons8.com/icon/2898/down-2",
                "https://icons8.com/icon/11511/reply-arrow",
                "https://icons8.com/icon/11511/reply-arrow",
                "https://icons8.com/icon/49235/helicopter",
                "https://icons8.com/icon/253/helicopter",
                "https://icons8.com/icon/42324/car",
                "https://icons8.com/icon/4699/cathedral",
                "https://icons8.com/icon/46873/teddy-bear"

        };

        String infoText="http://icons8.com/tag/media-controls/" +
                "https://icons8.com/icon/2770/vacuum-cleaner" +
                "https://icons8.com/icon/24750/blender" +
                "https://icons8.com/icon/13885/dishwasher" +
                "https://icons8.com/icon/49380/washing-machine" +
                "https://icons8.com/icon/549/washing-machine" +
                "https://icons8.com/icon/71/hair-dryer" +
                "https://icons8.com/icon/71/hair-dryer#filled" +
                "https://icons8.com/icon/54374/hair-dryer" +
                "https://icons8.com/icon/4455/bee" +
                "https://icons8.com/icon/5064/falcon" +
                "https://icons8.com/icon/44131/parrot" +
                "https://icons8.com/icon/36450/owl" +
                "https://icons8.com/icon/12695/bird#filled" +
                "https://icons8.com/icon/12695/bird" +
                "https://icons8.com/icon/16481/stork" +
                "https://icons8.com/icon/3104/fire-element" +
                "https://icons8.com/icon/2512/campfire" +
                "https://icons8.com/icon/2512/campfire#filled" +
                "https://icons8.com/icon/794/leaf" +
                "https://icons8.com/icon/794/leaf#filled" +
                "https://icons8.com/icon/2427/waterfall" +
                "https://icons8.com/icon/2427/waterfall#filled" +
                "https://icons8.com/icon/6600/grasshopper" +
                "https://icons8.com/icon/18582/knitted-frog" +
                "https://icons8.com/icon/48737/leaf" +
                "https://icons8.com/icon/2497/forest" +
                "https://icons8.com/icon/2497/forest#filled" +
                "https://icons8.com/icon/3023/sea-waves" +
                "https://icons8.com/icon/3023/sea-waves#filled" +
                "https://icons8.com/icon/23756/sea-waves" +
                "https://icons8.com/icon/3611/rainy-weather" +
                "https://icons8.com/icon/3611/rainy-weather#filled" +
                "https://icons8.com/icon/4819/plumbing" +
                "https://icons8.com/icon/9141/shower#filled" +
                "https://icons8.com/icon/9141/shower" +
                "https://icons8.com/icon/3432/wet" +
                "https://icons8.com/icon/4886/piping" +
                "https://icons8.com/icon/5289/field" +
                "https://icons8.com/icon/5289/field#filled" +
                "https://icons8.com/icon/2620/creek" +
                "https://icons8.com/icon/2620/creek#filled" +
                "https://icons8.com/icon/2638/keep-dry#filled" +
                "https://icons8.com/icon/2638/keep-dry" +
                "https://icons8.com/icon/11489/jacuzzi" +
                "https://icons8.com/icon/11485/shower-and-tub" +
                "https://icons8.com/icon/53931/shower-and-tub" +
                "https://icons8.com/icon/5008/wharf" +
                "https://icons8.com/icon/18564/intense-rain" +
                "https://icons8.com/icon/18565/moderate-rain" +
                "https://icons8.com/icon/51911/steam-engine" +
                "https://icons8.com/icon/7358/light-rain" +
                "https://icons8.com/icon/18563/heavy-rain" +
                "https://icons8.com/icon/49635/rain" +
                "https://icons8.com/icon/836/chance-of-storm" +
                "https://icons8.com/icon/1770/hydroelectric" +
                "https://icons8.com/icon/4461/dragonfly" +
                "https://icons8.com/icon/5291/hay" +
                "https://icons8.com/icon/398/play"+
                "https://icons8.com/icon/11504/forward-arrow"+
                "https://icons8.com/icon/2898/down-2"+
                "<a href=\"https://icons8.com/icon/5291/Hay\">Hay icon credits</a> ";


//        TextView link = (TextView) findViewById(R.id.textView1);
//        String linkText = "Visit the <a href='http://stackoverflow.com'>StackOverflow</a> web page.";
//        link.setText(Html.fromHtml(linkText));
        //link.setMovementMethod(LinkMovementMethod.getInstance());

        final TextView[] myTextViews = new TextView[linki.length]; // create an empty array;

        for (int i = 0; i < linki.length; i++) {
            // create a new textview
            final TextView rowTextView = new TextView(this);

            // set some properties of rowTextView or something
            String linkText = "Visit the <a href='"+linki[i]+"'>"+linki[i]+"</a> web page.";
            rowTextView.setText(linki[i]);
            rowTextView.setText(Html.fromHtml(linkText));
            rowTextView.setMovementMethod(LinkMovementMethod.getInstance());

            // add the textview to the linearlayout
            infoLL.addView(rowTextView);

            // save a reference to the textview for later
            myTextViews[i] = rowTextView;
        }
    }
}
