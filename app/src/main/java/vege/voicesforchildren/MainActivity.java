package vege.voicesforchildren;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.provider.MediaStore;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Switch;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static vege.voicesforchildren.FirstActivity.nameInt;
import static vege.voicesforchildren.FirstActivity.stringsOfImages;
import static vege.voicesforchildren.MusicService.setSound;


public class MainActivity extends AppCompatActivity {

    private static MediaPlayer mp;
    private static List<String> allVoicesName=new ArrayList<>(); //lista wszystkich dzwiekow
    private static String voice; //nazwa dzwieku aktualnie odtwarzanego

    public static final String MY_PREFS = "MUSIC_FOR";
    public static final String MUSIC = "MusicKey";

    private Context context;
    private int id1, iName;
    private String backgroundColor;

    private static boolean isStarted;
    private static int timeToFinish;
    protected static int step;
    private static final int MINUTA=60000;
    protected static int maxVolume;
    private static int setMinutes;
    private InterstitialAd mInterstitialAd;

    //@BindView(R.id.textView)TextView textView;
    @BindView(R.id.button) ImageView button;
    //@BindView(R.id.button) ImageView imageButton;
    @BindView(R.id.imageButton2) ImageView imageButton2;
    @BindView(R.id.radioGroup) RadioGroup radioGroup;
    @BindView(R.id.imageView2)ImageView imageView2;
    //@BindView(R.id.textView2)TextView textView2;
    @BindView(R.id.textView24)TextView textView24;
    @BindView(R.id.ly)RelativeLayout ly;
    @BindView(R.id.mainLayout)ScrollView mainLayout;
    @BindView(R.id.switch1)Switch switch1;

    protected static int name;

    RadioGroup rg;

    SharedPreferences sharedPreferences;
    Intent i;
    @BindView(R.id.progressBar)ProgressBar progressBar;
    protected static int numberOfSound;
    private static CountDownTimer countDownTimer;
    private boolean phoneDevice = true;
    protected static String sa;
    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        if(isStarted){
            button.setImageDrawable(getResources().getDrawable(R.drawable.stop));
            //start();
        }

        context=MainActivity.this;

        setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        ActionBar actionBar;
        actionBar = getSupportActionBar();
        actionBar.hide();
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-1487040786379130/5119163961");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        mAdView = (AdView) findViewById(R.id.adView2);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        for(int j=0;j<FirstActivity.name.size();j++){
            iName = context.getResources().getIdentifier(FirstActivity.name.get(j), "drawable", context.getPackageName());

            //nameInt.add(iName);
        }


        sa=FirstActivity.getVoicesList().get(getNumberOfSound()).getSoundImage().toString();
          //  textView2.setText(sa);
        id1 = context.getResources().getIdentifier(sa, "drawable", context.getPackageName());
        imageView2.setImageResource(id1);
        setMinutes=15;
        textView24.setText(name);

        ly.setMinimumHeight(FirstActivity.height*45/100);

       // ly.(FirstActivity.height*25/100);
        ly.setMinimumWidth(FirstActivity.height*45/100);
        ly.setGravity(Gravity.CENTER);

        rg = (RadioGroup)findViewById(R.id.radioGroup);
        String radiovalue = ((RadioButton)findViewById(rg.getCheckedRadioButtonId())).getText().toString();
       // textView2.setText(radiovalue);
        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                switch(checkedId)
                {
                    case R.id.radioButton4:
                        setMinutes=240;
                        break;
                    case R.id.radioButton9:
                        setMinutes=480;
                        break;
                    case R.id.radioButton5:
                        setMinutes=120;
                        break;
                    case R.id.radioButton6:
                        setMinutes=60;
                        break;
                    case R.id.radioButton7:
                        setMinutes=30;
                        break;
                    case R.id.radioButton8:
                        setMinutes=15;
                        break;
                }
                //textView2.setText(Integer.toString(setMinutes));
                timeToFinish=MINUTA*setMinutes;
                progressBar.setMax(timeToFinish/MINUTA);
                maxVolume=setMinutes;
                setParams();
                isStarted=false;
                switch1.setClickable(true);
                button.setImageDrawable(getResources().getDrawable(R.drawable.play));
            }
        });

        checkVisivility();


        progressBar.setProgress(0);
//        textView24.setText(nameInt.get(numberOfSound));
        timeToFinish=MINUTA*setMinutes;
        maxVolume=setMinutes;
        setBackground();
        step=0;
        i = new Intent(this, MusicService.class);
        progressBar.setMax(timeToFinish/MINUTA);
        sharedPreferences = getSharedPreferences(MY_PREFS, Context.MODE_PRIVATE);


        String a=FirstActivity.name.get(numberOfSound);
        int resId = getResources().getIdentifier(a, "string", getPackageName());
        textView24.setText(resId);

    }
    @OnClick(R.id.button)
    public void start(){
            //sharedPreferences.edit().putBoolean(MUZYKA, true).apply();
        if(!isStarted) {
            switch1.setClickable(false);
            button.setImageDrawable(getResources().getDrawable(R.drawable.stop));
            progressBar.setProgress(0);
            step=0;
            isStarted=true;
            //Intent i = new Intent(this, MusicService.class);
            startService(i);

            try {
                countDownTimer = new CountDownTimer(timeToFinish, MINUTA) {


                    public void onTick(long millisUntilFinished) {
                        progressBar.setProgress(step);
                        if(switch1.isChecked()) {
                            setSound();
                        }
                        step++;


                        if(step==(timeToFinish/MINUTA)-2){
                            mInterstitialAd.show();
                        }


                        // mTextField.setText("seconds remaining: " + millisUntilFinished / 1000);
                    }

                    public void onFinish() {
                        stopService(i);
                        progressBar.setProgress(timeToFinish);
                        isStarted = false;
                    }
                }.start();
            }catch (IllegalStateException e){
                //obsłużyć
            }
        }
        else{
            button.setImageDrawable(getResources().getDrawable(R.drawable.play));
            isStarted=false;
            stopService(i);
            progressBar.setProgress(timeToFinish);
            checkVisivility();
            switch1.setClickable(true);
        }
    }
    @OnClick(R.id.imageButton2)
    public void nextSound(){


        numberOfSound++;
        checkVisivility();
        setParams();
       // textView.setText(Integer.toString(numberOfSound));
        textView24.setText(name);
        isStarted=false;
        setBackground();
        button.setImageDrawable(getResources().getDrawable(R.drawable.play));
        switch1.setClickable(true);


        String a=FirstActivity.name.get(numberOfSound);
        int resId = getResources().getIdentifier(a, "string", getPackageName());
        textView24.setText(resId);

    }

    @OnClick(R.id.imageButton)
    public void previousSound(){


        numberOfSound--;
        checkVisivility();
        setParams();
        setBackground();
       // textView.setText(Integer.toString(numberOfSound));
        isStarted=false;
        setBackground();
        button.setImageDrawable(getResources().getDrawable(R.drawable.play));
        switch1.setClickable(true);


        String a=FirstActivity.name.get(numberOfSound);
        int resId = getResources().getIdentifier(a, "string", getPackageName());
        textView24.setText(resId);

    }

    private void setParams() {
        stopService(i);
        if(countDownTimer!=null)countDownTimer.cancel();
        progressBar.setProgress(0);
        sa=FirstActivity.getVoicesList().get(getNumberOfSound()).getSoundImage().toString();
        id1 = context.getResources().getIdentifier(sa, "drawable", context.getPackageName());
        imageView2.setImageResource(id1);
    }

    public  void checkVisivility(){
        if (getNumberOfSound()<0){
            setNumberOfSound(FirstActivity.getVoicesList().size()-1);

        }
        else if(numberOfSound>=FirstActivity.getVoicesList().size()){
            setNumberOfSound(0);

        }

    }

    public  void setGraphic(String s){
        int backgroundGraphic = context.getResources().getIdentifier("background"+s, "raw", context.getPackageName());
        int imageGraphic = context.getResources().getIdentifier(s, "raw", context.getPackageName());
    }
    public void minimizeApp() {
        Intent startMain = new Intent(Intent.ACTION_MAIN);
        startMain.addCategory(Intent.CATEGORY_HOME);
        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(startMain);
    }
    @OnClick(R.id.button2)
    public void min(){
        minimizeApp();
    }

    public static List<String> getAllVoicesName() {
        return allVoicesName;
    }

    public static void setAllVoicesName(List<String> allVoicesName) {
        MainActivity.allVoicesName = allVoicesName;
    }

    public static String getVoice() {
        return voice;
    }

    public static void setVoice(String voice) {
        MainActivity.voice = voice;
    }

    public static int getNumberOfSound() {
        return numberOfSound;
    }

    public static void setNumberOfSound(int numberOfSound) {
        MainActivity.numberOfSound = numberOfSound;
    }

    public void setBackground(){

       // textView24.setText(imageView2.g.toString());
        //char firstLetter=MusicService.nazwa.charAt(0);
        switch (stringsOfImages.get(numberOfSound).toString().charAt(0)){
            case 'a':
                mainLayout.setBackgroundColor((getResources().getColor(R.color.agd)));
                break;
            case 'b':
                mainLayout.setBackgroundColor((getResources().getColor(R.color.woda)));
                break;
            case 'c':
                mainLayout.setBackgroundColor((getResources().getColor(R.color.ogien)));
                break;
            case 'd':
                mainLayout.setBackgroundColor((getResources().getColor(R.color.laka)));
                break;
            case 'e':
                mainLayout.setBackgroundColor((getResources().getColor(R.color.las)));
                break;
            case 'f':
                mainLayout.setBackgroundColor((getResources().getColor(R.color.pojazdy)));
                break;
            case 'g':
                mainLayout.setBackgroundColor((getResources().getColor(R.color.inne)));
                break;
        }

    }


}