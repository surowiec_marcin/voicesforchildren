package vege.voicesforchildren;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Vege on 04.08.2017.
 */

public class Voices {
    //static List<String> voice=new ArrayList<>();
//    static String[] v={"dryer1", "dryer2"};
//    public Voices(){
//        Collections.addAll(voice, v);
//    }
//    public static String setVoice(int i){
//        return v[i];
//    }
//    public static int getSize(){
//        return v.length;
//    }

    private String soundImage;
    private String soundName;

    public Voices(String soundImage, String soundName ) {
        this.soundImage = soundImage;
        this.soundName = soundName;
    }

    public String getSoundImage() {
        return soundImage;
    }

    public void setSoundImage(String soundImage) {
        this.soundImage = soundImage;
    }

    public String getSoundName() {
        return soundName;
    }

    public void setSoundName(String soundName) {
        this.soundName = soundName;
    }
}
