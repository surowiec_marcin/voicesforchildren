package vege.voicesforchildren;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FirstActivity extends AppCompatActivity implements View.OnLongClickListener{

    @BindView(R.id.imageView15)ImageView imageView15;
    @BindView(R.id.imageView16)ImageView imageView16;
    @BindView(R.id.imageView17)ImageView imageView17;
    @BindView(R.id.imageView18)ImageView imageView18;
    @BindView(R.id.imageView19)ImageView imageView19;
    @BindView(R.id.imageView20)ImageView imageView20;
    @BindView(R.id.imageView21)ImageView imageView21;
    @BindView(R.id.imageView22)ImageView imageView22;
    @BindView(R.id.imageView23)ImageView imageView23;
  //  @BindView(R.id.imageView24)ImageView imageView24;
  //  @BindView(R.id.imageView15)ImageView imageView25;
   // @BindView(R.id.imageView15)ImageView imageView25;
  //  @BindView(R.id.imageView15)ImageView imageView26;
    @BindView(R.id.imageView27)ImageView imageView27;
    @BindView(R.id.imageView28)ImageView imageView28;
    @BindView(R.id.imageView29)ImageView imageView29;
    @BindView(R.id.imageView30)ImageView imageView30;
    @BindView(R.id.imageView31)ImageView imageView31;
    @BindView(R.id.imageView32)ImageView imageView32;
    @BindView(R.id.imageView33)ImageView imageView33;
    @BindView(R.id.imageView34)ImageView imageView34;
    @BindView(R.id.imageView35)ImageView imageView35;
    @BindView(R.id.imageView36)ImageView imageView36;
    @BindView(R.id.imageView37)ImageView imageView37;
    @BindView(R.id.imageView38)ImageView imageView38;
    @BindView(R.id.imageView39)ImageView imageView39;
    @BindView(R.id.imageView40)ImageView imageView40;
    @BindView(R.id.imageView41)ImageView imageView41;
    @BindView(R.id.imageView42)ImageView imageView42;
    @BindView(R.id.imageView43)ImageView imageView43;
    @BindView(R.id.imageView44)ImageView imageView44;
    @BindView(R.id.imageView45)ImageView imageView45;
    @BindView(R.id.imageView46)ImageView imageView46;
    @BindView(R.id.imageView47)ImageView imageView47;
    @BindView(R.id.imageView48)ImageView imageView48;
    @BindView(R.id.imageView49)ImageView imageView49;
    @BindView(R.id.imageView50)ImageView imageView50;
    @BindView(R.id.imageView51)ImageView imageView51;
    @BindView(R.id.imageView52)ImageView imageView52;
    @BindView(R.id.imageView53)ImageView imageView53;
    @BindView(R.id.imageView54)ImageView imageView54;
    @BindView(R.id.imageView55)ImageView imageView55;
    @BindView(R.id.imageView56)ImageView imageView56;
    @BindView(R.id.imageView57)ImageView imageView57;
    @BindView(R.id.imageView58)ImageView imageView58;
    @BindView(R.id.imageView59)ImageView imageView59;
    @BindView(R.id.imageView60)ImageView imageView60;
    @BindView(R.id.imageView61)ImageView imageView61;
    @BindView(R.id.imageView62)ImageView imageView62;
    @BindView(R.id.imageView63)ImageView imageView63;
    @BindView(R.id.imageView64)ImageView imageView64;
    @BindView(R.id.imageView65)ImageView imageView65;

    protected static int width;
    protected static int height;

    private boolean phoneDevice = true;

    private static List<String> music;
    protected static List<String> stringsOfImages;
    protected static List<String> name;
    protected static List<Integer> nameInt;

    private boolean longClickActivated;
    private Map<ImageView, String> playlist=new HashMap<>();

    private AdView mAdView;
    Context context;

    private static List<Voices> voicesList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);
        ButterKnife.bind(this);

       MobileAds.initialize(this, "ca-app-pub-1487040786379130~4617790452");
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);




        List<ImageView> arrayOfImages = Arrays.asList(imageView15, imageView16, imageView17, imageView18,imageView19,imageView20,imageView21
        ,imageView22,imageView23,imageView27,imageView28,imageView29,imageView30,imageView31,imageView32,imageView33,imageView34,imageView35,imageView36
        ,imageView37,imageView38,imageView39,imageView40,imageView41,imageView42,imageView43,imageView44,imageView45,imageView46,imageView47,imageView48
        ,imageView49,imageView50,imageView51,imageView52,imageView53,imageView54,imageView55,imageView56,imageView57,imageView58,imageView59,imageView60
                ,imageView61,imageView62,imageView63,imageView64,imageView65 );

        List<ImageView> listOfImages =new ArrayList<>();
        listOfImages.addAll(arrayOfImages);

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        FirstActivity.width = size.x;
        FirstActivity.height = size.y;

        for(int i =0;i<listOfImages.size();i++){
            setSizeOfImage(listOfImages.get(i));
        }

        music= Arrays.asList("a001","a002","a003","a004","a005","a006","a007","a008","a009",
"b001","b002","b003","b004","b005","b006","b007","b008","b009", "b010","b011","b012","b013","b014","b015",
"c001","c002","c003",
"d001","d002","d003",
"e001","e002","e003","e004","e005","e006","e007","e008","e009", "e010","e011","e012",
"f001","f002","f003",
"g001","g002","g003");

        stringsOfImages=music;
//        stringsOfImages= Arrays.asList("a001_vacum","a002_fan_50","a003_blender_50","a004_dishwasher",
//               "a005_washing_machine3","a006_washing_mashine","a007_hair_dryer_50","a008_dryer1","a009_hair_dryer3" );



        name=Arrays.asList("Odkurzacz","Wentylator", "mikser", "Zmywarka", "Dryer", "Pralka","suszarka1" ,
"suszarka2","suszarka3","Fale1","Fontanna","Fale2","Ocean","Strumien1","Strumien2",
"Prysznic1","Kran","Prysznic2","Deszcz1","Deszcz2","Deszcz3","Burza","Wodospad1","Wodospad2","Ogien1",
"Ogien2","Ogien3","Zaba","Swierszcz","Pszczola","Ptak1","Ptak2","Ptak3","Sowa","Ptak4","Ptak5","Las1",
"Las2","Las3","Las4","Las5","Las6","Pociag","Samochod","Helikopter","Katedra","Owca","Ciuchcia");


        voicesList=new ArrayList<>();
        for(int i=0;i<music.size();i++){
            Voices v=new Voices(stringsOfImages.get(i),music.get(i));
            voicesList.add(v);
        }



        setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


    }

    public boolean onCreateOptionsMenu(Menu menu) {
        // ustal aktualną orientację urządzenia
        int orientation = getResources().getConfiguration().orientation;

        // wyświetla menu aplikacji tylko w orientacji pionowej
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            // wygeneruj menu
            getMenuInflater().inflate(R.menu.menu_info, menu);
            return true;
        }
        else
            return false;
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.action_info) {
            Intent preferencesIntent = new Intent(getApplicationContext(), Info.class);
            startActivity(preferencesIntent);
        }
        return false;
    }

    public static void setSizeOfImage(ImageView imageView){
        imageView.setAdjustViewBounds(true);
        imageView.setMaxHeight(FirstActivity.height*25/100);
        imageView.setAdjustViewBounds(true);
        imageView.setMinimumHeight(FirstActivity.height*25/100);
        imageView.setMaxWidth(FirstActivity.height*25/100);
        imageView.setMinimumWidth(FirstActivity.height*25/100);
    }

    public static List<Voices> getVoicesList() {
        return voicesList;
    }

    public static void setVoicesList(List<Voices> voicesList) {
        FirstActivity.voicesList = voicesList;
    }

    @OnClick(R.id.imageView17)
    public void open17(){
        MainActivity.setNumberOfSound(0);
        moveImage(imageView17);
        MainActivity.name = R.string.Odkurzacz;
    }
    @OnClick(R.id.imageView16)
    public void open16(){
        MainActivity.setNumberOfSound(1);
        MainActivity.name = R.string.Wentylator;
        moveImage(imageView16);
    }
    @OnClick(R.id.imageView15)
    public void open15(){
        MainActivity.setNumberOfSound(2);
        MainActivity.name = R.string.mikser;
        moveImage(imageView15);
    }
    @OnClick(R.id.imageView20)
    public void open18(){
        MainActivity.setNumberOfSound(3);
        MainActivity.name = R.string.Zmywarka;
        moveImage(imageView20);
    }
    @OnClick(R.id.imageView19)
    public void open19(){
        MainActivity.setNumberOfSound(4);
        MainActivity.name = R.string.Suszarka;
        moveImage(imageView19);
    }
    @OnClick(R.id.imageView18)
    public void open20(){
        MainActivity.setNumberOfSound(5);
        MainActivity.name = R.string.Pralka;
        moveImage(imageView18);
    }
    @OnClick(R.id.imageView23)
    public void open21(){
        MainActivity.setNumberOfSound(6);
        MainActivity.name = R.string.Wentylator;
        moveImage(imageView23);
    }
    @OnClick(R.id.imageView22)
    public void open22(){
        MainActivity.setNumberOfSound(7);
        MainActivity.name = R.string.Wentylator;
        moveImage(imageView22);
    }
    @OnClick(R.id.imageView21)
    public void open23(){
        MainActivity.setNumberOfSound(8);
        MainActivity.name = R.string.Wentylator;
        moveImage(imageView21);
    }
    @OnClick(R.id.imageView29)
    public void open27(){
        MainActivity.setNumberOfSound(9);
        MainActivity.name = R.string.Wentylator;
        moveImage(imageView29);
    }
    @OnClick(R.id.imageView28)
    public void open28(){
        MainActivity.setNumberOfSound(10);
        MainActivity.name = R.string.Wentylator;
        moveImage(imageView28);
    }
    @OnClick(R.id.imageView27)
    public void open29(){
        MainActivity.setNumberOfSound(11);
        MainActivity.name = R.string.Wentylator;
        moveImage(imageView27);
    }
    @OnClick(R.id.imageView32)
    public void open30(){
        MainActivity.setNumberOfSound(12);
        MainActivity.name = R.string.Wentylator;
        moveImage(imageView32);
    }
    @OnClick(R.id.imageView31)
    public void open31(){
        MainActivity.setNumberOfSound(13);
        MainActivity.name = R.string.Wentylator;
        moveImage(imageView31);
    }
    @OnClick(R.id.imageView30)
    public void open32(){
        MainActivity.setNumberOfSound(14);
        MainActivity.name = R.string.Wentylator;
        moveImage(imageView30);
    }
    @OnClick(R.id.imageView35)
    public void open33(){
        MainActivity.setNumberOfSound(15);
        MainActivity.name = R.string.Wentylator;
        moveImage(imageView35);
    }
    @OnClick(R.id.imageView34)
    public void open34(){
        MainActivity.setNumberOfSound(16);
        MainActivity.name = R.string.Wentylator;
        moveImage(imageView34);
    }
    @OnClick(R.id.imageView33)
    public void open35(){
        MainActivity.setNumberOfSound(17);
        MainActivity.name = R.string.Wentylator;
        moveImage(imageView33);
    }
    @OnClick(R.id.imageView38)
    public void open36(){
        MainActivity.setNumberOfSound(18);
        MainActivity.name = R.string.Wentylator;
        moveImage(imageView38);
    }
    @OnClick(R.id.imageView37)
    public void open37(){
        MainActivity.setNumberOfSound(19);
        MainActivity.name = R.string.Wentylator;
        moveImage(imageView37);
    }
    @OnClick(R.id.imageView36)
    public void open38(){
        MainActivity.setNumberOfSound(20);
        MainActivity.name = R.string.Wentylator;
        moveImage(imageView36);
    }
    @OnClick(R.id.imageView41)
    public void open39(){
        MainActivity.setNumberOfSound(21);
        MainActivity.name = R.string.Wentylator;
        moveImage(imageView41);
    }
    @OnClick(R.id.imageView40)
    public void open40(){
        MainActivity.setNumberOfSound(22);
        MainActivity.name = R.string.Wentylator;
        moveImage(imageView40);
    }
    @OnClick(R.id.imageView39)
    public void open41(){
        MainActivity.setNumberOfSound(23);
        MainActivity.name = R.string.Wentylator;
        moveImage(imageView39);
    }
    @OnClick(R.id.imageView44)
    public void open42(){
        MainActivity.setNumberOfSound(24);
        MainActivity.name = R.string.Wentylator;
        moveImage(imageView44);
    }
    @OnClick(R.id.imageView43)
    public void open43(){
        MainActivity.setNumberOfSound(25);
        MainActivity.name = R.string.Wentylator;
        moveImage(imageView43);
    }
    @OnClick(R.id.imageView42)
    public void open44(){
        MainActivity.setNumberOfSound(26);
        MainActivity.name = R.string.Wentylator;
        moveImage(imageView42);
    }
    @OnClick(R.id.imageView45)
    public void open45(){
        MainActivity.setNumberOfSound(29);
        MainActivity.name = R.string.Wentylator;
        moveImage(imageView45);
    }
    @OnClick(R.id.imageView46)
    public void open46(){
        MainActivity.setNumberOfSound(28);
        MainActivity.name = R.string.Wentylator;
        moveImage(imageView46);
    }
    @OnClick(R.id.imageView47)
    public void open47(){
        MainActivity.setNumberOfSound(27);
        MainActivity.name = R.string.Wentylator;
        moveImage(imageView47);
    }

    @OnClick(R.id.imageView48)
    public void open48(){
        MainActivity.setNumberOfSound(32);
        MainActivity.name = R.string.Wentylator;
        moveImage(imageView48);
    }
    @OnClick(R.id.imageView51)
    public void open49(){
        MainActivity.setNumberOfSound(35);
        MainActivity.name = R.string.Wentylator;
        moveImage(imageView51);
    }
    @OnClick(R.id.imageView50)
    public void open50(){
        MainActivity.setNumberOfSound(30);
        MainActivity.name = R.string.Wentylator;
        moveImage(imageView50);
    }
    @OnClick(R.id.imageView49)
    public void open51(){
        MainActivity.setNumberOfSound(31);
        MainActivity.name = R.string.Wentylator;
        moveImage(imageView49);
    }
    @OnClick(R.id.imageView54)
    public void open52(){
        MainActivity.setNumberOfSound(38);
        MainActivity.name = R.string.Wentylator;
        moveImage(imageView54);
    }
    @OnClick(R.id.imageView53)
    public void open53(){
        MainActivity.setNumberOfSound(33);
        MainActivity.name = R.string.Wentylator;
        moveImage(imageView53);
    }
    @OnClick(R.id.imageView52)
    public void open54(){
        MainActivity.setNumberOfSound(34);
        MainActivity.name = R.string.Wentylator;
        moveImage(imageView52);
    }
    @OnClick(R.id.imageView56)
    public void open56(){
        MainActivity.setNumberOfSound(36);
        MainActivity.name = R.string.Wentylator;
        moveImage(imageView56);
    }
    @OnClick(R.id.imageView55)
    public void open55(){
        MainActivity.setNumberOfSound(37);
        MainActivity.name = R.string.Wentylator;
        moveImage(imageView55);
    }
    @OnClick(R.id.imageView59)
    public void open57(){
        MainActivity.setNumberOfSound(39);
        MainActivity.name = R.string.Wentylator;
        moveImage(imageView59);
    }
    @OnClick(R.id.imageView58)
    public void open58(){
        MainActivity.setNumberOfSound(40);
        MainActivity.name = R.string.Wentylator;
        moveImage(imageView58);
    }
    @OnClick(R.id.imageView57)
    public void open59(){
        MainActivity.setNumberOfSound(41);
        MainActivity.name = R.string.Wentylator;
        moveImage(imageView57);
    }
    @OnClick(R.id.imageView62)
    public void open60(){
        MainActivity.setNumberOfSound(42);
        MainActivity.name = R.string.Wentylator;
        moveImage(imageView62);
    }
    @OnClick(R.id.imageView61)
    public void open61(){
        MainActivity.setNumberOfSound(43);
        MainActivity.name = R.string.Wentylator;
        moveImage(imageView61);
    }
    @OnClick(R.id.imageView60)
    public void open62(){
        MainActivity.setNumberOfSound(44);
        MainActivity.name = R.string.Wentylator;
        moveImage(imageView60);
    }
    @OnClick(R.id.imageView65)
    public void open63(){
        MainActivity.setNumberOfSound(45);
        MainActivity.name = R.string.Wentylator;
        moveImage(imageView65);
    }
    @OnClick(R.id.imageView64)
    public void open64(){
        MainActivity.setNumberOfSound(46);
        MainActivity.name = R.string.Wentylator;
        moveImage(imageView64);
    }
    @OnClick(R.id.imageView63)
    public void open65(){
        MainActivity.setNumberOfSound(47);
        MainActivity.name = R.string.Wentylator;
        moveImage(imageView63);
    }

    public static List<String> getMusic() {
        return music;
    }

    public static void setMusic(List<String> music) {
        FirstActivity.music = music;
    }

    public static List<String> getStringsOfImages() {
        return stringsOfImages;
    }

    public static void setStringsOfImages(List<String> stringsOfImages) {
        FirstActivity.stringsOfImages = stringsOfImages;
    }

    @Override
    public boolean onLongClick(View view) {
        longClickActivated=true;
        switch (view.getId()){
            case R.id.imageView15:

                break;
            case R.id.imageView16:

                break;
        }
        return false;
    }
//    public void createPlaylist(View view){
//
//        playlist.
//
//    }

    public  void moveImage(final ImageView imageView) {
        int timeForWait = 1000;
        imageView.animate().scaleY(1.00f).scaleX(-1f).setInterpolator(new AccelerateDecelerateInterpolator()).setDuration(timeForWait);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {

                imageView.animate().scaleY(1f).scaleX(1f).rotationBy(0).translationX(0).translationY(0).setInterpolator(new AccelerateDecelerateInterpolator()).setDuration(1000);

                Intent myIntent = new Intent(FirstActivity.this, MainActivity.class);
                startActivity(myIntent);
               // imageView.setBackgroundColor(Color.DKGRAY);

            }
        }, timeForWait);
    }
}
