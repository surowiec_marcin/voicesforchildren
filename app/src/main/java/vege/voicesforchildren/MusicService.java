package vege.voicesforchildren;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import java.io.IOException;

import static vege.voicesforchildren.MainActivity.maxVolume;
import static vege.voicesforchildren.MainActivity.step;

public class MusicService extends Service {
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;

    }

    protected static String nazwa;
    protected static MediaPlayer mp;
    AssetFileDescriptor afd;
    AssetManager assetManager;




    @Override
    public void onCreate(){
        super.onCreate();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId){
        mp = new MediaPlayer();
        nazwa=FirstActivity.getVoicesList().get(MainActivity.numberOfSound).getSoundName().toString();
        Uri uri = Uri.parse("android.resource://" + getPackageName() + "/raw/" + nazwa);
        //int nazwaPliku = context.getResources().getIdentifier(nazwa, "raw", context.getPackageName());
        int checkExistence = getResources().getIdentifier(nazwa, "raw", getPackageName());


        if (checkExistence != 0) {
            try {

                mp.setDataSource(getApplicationContext(), uri);
//                afd = assetManager.openFd(nazwa);

            } catch (IOException e) {
                e.printStackTrace();
            }


//            try {
//                mp.setDataSource(afd.getFileDescriptor(),
//                        afd.getStartOffset(), afd.getLength() - 1000);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            try {
//                afd.close();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
            mp.setLooping(true);
            mp.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer player) {
                    player.start();
                }
            });
            mp.prepareAsync();
            mp.setVolume(1, 1);

        }
        return super.onStartCommand(intent, flags, startId);
    }



    @Override
    public void onDestroy(){
        super.onDestroy();

        mp.reset();
        mp.release();
    }
    public static void setSound(){
        float volume =(float) (((Math.log(maxVolume)-Math.log(step)) / Math.log(maxVolume)));
        try {
            mp.setVolume(volume, volume);
        }catch (IllegalStateException e){
            Log.e("MusicService", "Error with volume");
        }
    }

}
